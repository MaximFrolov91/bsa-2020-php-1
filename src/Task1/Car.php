<?php

declare(strict_types=1);

namespace App\Task1;

class Car
{
    public function __construct(
        int $id,
        string $image,
        string $name,
        int $speed,
        int $pitStopTime,
        float $fuelConsumption,
        float $fuelTankVolume
    ) {
        $this->id = $id;
        $this->image = $image;
        $this->name = $name;
        if ($speed >= 0) {
            $this->speed = $speed;
        }
        else throw new \Exception("speed < 0");
//        $this->pitStopTime = $pitStopTime;
        if ($pitStopTime >= 0) {
            $this->pitStopTime = $pitStopTime;
        }
        else throw new \Exception("pitStopTime < 0");
//        $this->fuelConsumption = $fuelConsumption;
        if ($fuelConsumption >= 0) {
            $this->fuelConsumption = $fuelConsumption;
        }
        else throw new \Exception("fuelConsumption < 0");
//        $this->fuelTankVolume = $fuelTankVolume;
        if ($fuelTankVolume >= 0) {
            $this->fuelTankVolume = $fuelTankVolume;
        }
        else throw new \Exception("fuelTankVolume < 0");
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSpeed(): int
    {
        return $this->speed;
    }

    public function getPitStopTime(): int
    {
        return $this->pitStopTime;
    }

    public function getFuelConsumption(): float
    {
        return $this->fuelConsumption;
    }

    public function getFuelTankVolume(): float
    {
        return $this->fuelTankVolume;
    }
}