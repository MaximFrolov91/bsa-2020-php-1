<?php

declare(strict_types=1);

namespace App\Task2;

class Book
{
    public function __construct(string $title, int $price, int $number)
    {
        $this->title = $title;
//        $this->price = $price;
        if ($price >= 0) {
            $this->price = $price;
        }
        else throw new \Exception("price < 0");
//        $this->number = $number;
        if ($number >= 0) {
            $this->number = $number;
        }
        else throw new \Exception("number < 0");
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPagesNumber(): int
    {
        return $this->number;
    }
}